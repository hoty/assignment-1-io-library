section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.count:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .count
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    call string_length
	pop rdi

	mov rsi, rdi
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
	mov rdi, rsp
	call print_string
	pop rdi    
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
	mov rdi, rsp
	push 0
	sub rsp, 16		; выделяем память
	dec rdi
	mov r8, 10
.loop:
	xor rdx, rdx
	div r8
	or rdx, 48
	dec rdi	
	mov [rdi], dl
	test rax, rax
	jnz .loop
	call print_string
	add rsp, 24		; освобождаем память
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
	jns print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte [rdi]
	cmp al, byte [rsi]
	jne .end_no
	inc rdi
	inc rsi
	test al, al
	jnz string_equals
	mov rax, 1
    ret
.end_no:
	xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
	xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r14
	push r15
	xor r14, r14
	mov r15, rsi
	dec r15
.first:
	push rdi
	call read_char
	pop rdi
	cmp al, ' '
	je .first
	cmp al, 9
	je .first
	cmp al, 10
	je .first
	cmp al, 13
	je .first
	test al, al
	jz .third
.second:
	mov byte [rdi + r14], al
	inc r14
	push rdi
	call read_char
	pop rdi
	cmp al, ' '
	je .third
	cmp al, 9
	je .third
	cmp al, 10
	je .third
	cmp al, 13
	je .third
	test al, al
	jz .third
	cmp r14, r15
	je .err
	jmp .second
.third:
	mov byte [rdi + r14], 0
	mov rax, rdi
	mov rdx, r14
	pop r15
	pop r14
    ret
.err:
	xor rax, rax
	pop r15
	pop r14
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8, 10
	xor rax, rax
	xor rcx, rcx
.loop:
	movzx r9, byte [rdi + rcx]
	cmp r9b, '0'
	jb .end
	cmp r9b, '9'
	ja .end
	xor rdx, rdx
	mul r8
	and r9b, 0xf
	add rax, r9
	inc rcx
	jmp .loop
.end:
	mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, byte [rdi]
	cmp al, '-'
	je .sign
	call parse_uint
	ret
.sign:
	inc rdi
	call parse_uint
	neg rax
	test rdx, rdx
	jz .err
	inc rdx
    ret
.err:
	xor rax, rax
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
	cmp rax, rdx
	jae .buff_ov
.loop:
	mov dl, byte [rdi]
	mov byte [rsi], dl
	inc rdi
	inc rsi
	test dl, dl
	jnz .loop
	ret
.buff_ov:
	xor rax, rax
    ret
